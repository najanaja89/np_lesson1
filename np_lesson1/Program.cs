﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace np_lesson1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region MyRegion
            //var getHostName = Dns.GetHostName();
            //Console.WriteLine("Host Name " + getHostName);

            ////Dns.GetHostEntry(getHostName);
            //Console.WriteLine("Enter Host Name");
            //getHostName = Console.ReadLine();
            ////Получение IP-адресов по имени хоста
            //var ipList = Dns.GetHostEntry(getHostName);
            ////Dns.GetHostByName(getHostName);
            //foreach (var name in ipList.AddressList)
            //{
            //    Console.WriteLine(name.ToString());
            //}

            //var ipList2 = Dns.GetHostEntry(ipList.AddressList[0]);
            //foreach (var ip in ipList2.Aliases)
            //{
            //    Console.WriteLine(ip.ToString());
            //}

            //Console.ReadLine(); 
            #endregion

            #region Client Creation
            //using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            //{
            //    //1 Вариант
            //    var hostName = "mail.ru";
            //    int hostport = 80;
            //    //socket.Connect(hostName, hostport);
            //    //socket.Shutdown(SocketShutdown.Both);
            //    //socket.Close();

            //    //2 Вариант
            //    var ipEntry = Dns.GetHostEntry(hostName);
            //    //Создание конечной точки удаленного хоста
            //    IPEndPoint endpoint = new IPEndPoint(ipEntry.AddressList[0], hostport);
            //    socket.Connect(endpoint);

            //    if (socket.Connected)
            //    {
            //        //Посылка сообщения на удаленый хост
            //        socket.Send(Encoding.UTF8.GetBytes(
            //            "GET / HTTP 1.1\r\n"
            //            ));//HTTP
            //        //Прием сообщения от удаленного хоста
            //        byte[] buf = new byte[4 * 1024];
            //        int recSize = socket.Receive(buf);

            //        Console.WriteLine($"Receive size {recSize}");
            //        Console.WriteLine(Encoding.UTF8.GetString(buf, 0, recSize));
            //        //Закрыть открытый канал связи
            //        socket.Shutdown(SocketShutdown.Both);
            //        socket.Close();
            //    }
            //    else
            //    {
            //        Console.WriteLine("Connection failed");
            //        return;
            //    }
            //    Console.ReadLine();
            //}
            #endregion

            using (var socketServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                string srvAddress = "0.0.0.0"; //Маска подсети - брать соединения у всех
                //string srvAddress = "127.0.0.1";
                //string srvAddress = "10.0.56.46";
                int srvPort = 12345;
                socketServer.Bind(new IPEndPoint(IPAddress.Parse(srvAddress), srvPort));
                socketServer.Listen(100);
              
                while (true)
                {
                    
                    Socket client = socketServer.Accept();
                    Console.WriteLine("Клиент подключился");
                    Console.WriteLine(client.RemoteEndPoint.ToString());
                    ThreadPool.QueueUserWorkItem(ClientThreadProc, client);
                }
            }
        }

        //Поток обслуживания клиента
        static void ClientThreadProc(object obj)
        {
            Socket client = (Socket)obj; //as Socket;
            try
            {
                //Протокол работы сервера - эхо-сервер
                byte[] buf = new byte[1024];
                while (true)
                {
                    int reccSize = client.Receive(buf);
                    Console.WriteLine($"Receive {reccSize} byte");
                    Console.WriteLine(Encoding.UTF8.GetString(buf,0 ,reccSize));
                    client.Send(buf, reccSize, SocketFlags.None);
                }
            }

            catch(Exception exception)
            {
             Console.WriteLine(exception.Message);
            }
            client.Shutdown(SocketShutdown.Both);
            client.Close();
        }
    }
}
